var StarShip = function ()
{
    this.canvas = document.getElementById('game-canvas'),
            this.context = this.canvas.getContext('2d'),
            this.fpsElement = document.getElementById('fps'),
            this.gameStarted = false,
            this.scoreElement = document.getElementById('score'),
            this.loadingElement = document.getElementById('loading'),
            this.loadingTitleElement = document.getElementById('loading-title'),
            this.AnimatedGifElement = document.getElementById('loading-gif'),
            this.toastElement = document.getElementById('toast'),
            this.instructionsElement = document.getElementById('instructions'),
            this.copyrightElement = document.getElementById('copyright'),
            
            this.TRACK_1_BASELINE = 323,
            this.TRACK_2_BASELINE = 223,
            this.TRACK_3_BASELINE = 123,
            this.background = new Image(),
            this.paused = false,
            this.PAUSED_CHECK_INTERVAL = 200,
            this.serverAvailable = true;
    
            this.MAX_NUMBER_OF_LIVES = 3;
            this.lives = this.MAX_NUMBER_OF_LIVES;
                        
            this.mobileInstructionsVisible = false,
            this.mobileStartToast = 
                     document.getElementById('starship-mobile-start-toast'),
            this.mobileWelcomeToast = 
                     document.getElementById('starship-mobile-welcome-toast'),
            this.welcomeStartLink = 
                     document.getElementById('starship-welcome-start-link'),
            this.showHowLink = 
                     document.getElementById('starship-show-how-link'),
            this.mobileStartLink = 
                     document.getElementById('starship-mobile-start-link'),
                     
            this.soundAndMusicElement = document.getElementById('sound-and-music'),
            this.musicElement = document.getElementById('starship-music'),
            this.musicElement.volume = 0.1,
            this.musicCheckboxElement = document.getElementById('music-checkbox'),
            this.soundCheckboxElement = document.getElementById('sound-checkbox'),
            this.musicOn = this.musicCheckboxElement.checked,
           // this.soundOn = this.soundCheckboxElement.checked,
            this.audioChannels = [
               {playing: false, audio: this.audioSprites},
               {playing: false, audio: null},
               {playing: false, audio: null},
               {playing: false, audio: null}],

            this.audioSpriteCountdown = this.audioChannels.length - 1,
            this.graphicsReady = false,
   
            this.LEFT = 1;
            this.RIGHT = 2;
            this.CAT_PACE_VELOCITY = 100;
            this.DOG_PACE_VELOCITY = 100;
            this.SHORT_DELAY = 50;
            this.timeRate = 1.0;
            this.timeSystem = new TimeSystem();

            this.SHIP_EXPLOSION_DURATION = 500;
            this.BAD_GUYS_EXPLOSION_DURATION = 1500;

            this.SHIP_LEFT = 50;
            this.STARTING_SHIP_TRACK = 1;
            this.TRANSPARENT = 0;
            this.OPAQUE = 1.0;
            
    try
    {
        this.socket = new io.connect('http://localhost:49800');
    } catch (err)
    {
        this.serverAvailable = false;
    }


    this.spritesheet = new Image(),
            this.shipspritesheet = new Image(),
            this.spriteOffset = 0,
            this.SHIP_CELLS_WIDTH = 340;
    this.SHIP_CELLS_HEIGHT = 582;

    this.SHOOT_CELLS_WIDTH = 58;
    this.SHOOT_CELLS_HEIGHT = 22;

    this.EXPLOSION_CELLS_HEIGHT = 62;

    this.CAT_CELLS_WIDTH = 186;
    this.CAT_CELLS_HEIGHT = 156;

    this.DOG_CELLS_WIDTH = 102;
    this.DOG_CELLS_HEIGHT = 235;

    this.ASTROID_CELLS_WIDTH = 111;
    this.ASTROID_CELLS_HEIGHT = 101;

    this.shipCells =
            [
                {left: 41, top: 79, width: this.SHIP_CELLS_WIDTH, height: this.SHIP_CELLS_HEIGHT},
                {left: 420, top: 79, width: this.SHIP_CELLS_WIDTH, height: this.SHIP_CELLS_HEIGHT},
                {left: 797, top: 79, width: this.SHIP_CELLS_WIDTH, height: this.SHIP_CELLS_HEIGHT},
                {left: 1214, top: 79, width: this.SHIP_CELLS_WIDTH, height: this.SHIP_CELLS_HEIGHT}


            ];
    this.shipLeftCells =
            [
                {left: 1568, top: 79, width: this.SHIP_CELLS_WIDTH, height: this.SHIP_CELLS_HEIGHT}
            ];

    this.shipRightCells =
            [
                {left: 120, top: 677, width: this.SHIP_CELLS_WIDTH, height: this.SHIP_CELLS_HEIGHT}

            ];

    this.shootBlueCells =
            [
                {left: 83, top: 616, width: this.SHOOT_CELLS_WIDTH, height: this.SHOOT_CELLS_HEIGHT},
                {left: 130, top: 616, width: this.SHOOT_CELLS_WIDTH, height: this.SHOOT_CELLS_HEIGHT},
                {left: 198, top: 616, width: this.SHOOT_CELLS_WIDTH, height: this.SHOOT_CELLS_HEIGHT}

            ];

    this.shootBlueCells =
            [
                {left: 95, top: 651, width: this.SHOOT_CELLS_WIDTH, height: this.SHOOT_CELLS_HEIGHT},
                {left: 139, top: 653, width: this.SHOOT_CELLS_WIDTH, height: this.SHOOT_CELLS_HEIGHT},
                {left: 195, top: 653, width: this.SHOOT_CELLS_WIDTH, height: this.SHOOT_CELLS_HEIGHT}

            ];

    this.catCells =
            [
                {left: 30, top: 46, width: this.CAT_CELLS_WIDTH, height: this.CAT_CELLS_HEIGHT},
                {left: 249, top: 46, width: this.CAT_CELLS_WIDTH, height: this.CAT_CELLS_HEIGHT}

            ];

    this.dogCells =
            [
                {left: 65, top: 265, width: this.DOG_CELLS_WIDTH, height: this.DOG_CELLS_HEIGHT},
                {left: 235, top: 265, width: this.DOG_CELLS_WIDTH, height: this.DOG_CELLS_HEIGHT},
                {left: 397, top: 265, width: this.DOG_CELLS_WIDTH, height: this.DOG_CELLS_HEIGHT}
            ];
    this.astroidCells =
            [
                {left: 74, top: 733, width: this.ASTROID_CELLS_WIDTH, height: this.ASTROID_CELLS_HEIGHT},
                {left: 221, top: 733, width: this.ASTROID_CELLS_WIDTH, height: this.ASTROID_CELLS_HEIGHT},
                {left: 366, top: 733, width: this.ASTROID_CELLS_WIDTH, height: this.ASTROID_CELLS_HEIGHT},
                {left: 514, top: 733, width: this.ASTROID_CELLS_WIDTH, height: this.ASTROID_CELLS_HEIGHT}
            ];

    this.explosionCells = [
        {left: 88, top: 540,
            width: 52, height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 151, top: 519,
            width: 70, height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 227, top: 521,
            width: 70, height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 314, top: 518,
            width: 70, height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 392, top: 519,
            width: 70, height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 479, top: 513,
            width: 70, height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 559, top: 516,
            width: 70, height: this.EXPLOSION_CELLS_HEIGHT}
    ];

    this.catData = [
        {left: 85,
            top: this.TRACK_2_BASELINE - 1.5 * this.CAT_CELLS_HEIGHT},
        {left: 620,
            top: this.TRACK_3_BASELINE},
        {left: 904,
            top: this.TRACK_3_BASELINE - 3 * this.CAT_CELLS_HEIGHT},
        {left: 1150,
            top: this.TRACK_2_BASELINE - 3 * this.CAT_CELLS_HEIGHT},
        {left: 1720,
            top: this.TRACK_2_BASELINE - 2 * this.CAT_CELLS_HEIGHT},
        {left: 1960,
            top: this.TRACK_3_BASELINE - this.CAT_CELLS_HEIGHT},
        {left: 2200,
            top: this.TRACK_3_BASELINE - this.CAT_CELLS_HEIGHT},
        {left: 2380,
            top: this.TRACK_3_BASELINE - 2 * this.CAT_CELLS_HEIGHT}
    ];
    this.dogData = [
        {left: 200,
            top: this.TRACK_1_BASELINE - this.DOG_CELLS_HEIGHT * 1.5},
        {left: 350,
            top: this.TRACK_2_BASELINE - this.DOG_CELLS_HEIGHT * 1.5},
        {left: 550,
            top: this.TRACK_1_BASELINE - this.DOG_CELLS_HEIGHT},
        {left: 750,
            top: this.TRACK_1_BASELINE - this.DOG_CELLS_HEIGHT * 1.5},
        {left: 924,
            top: this.TRACK_2_BASELINE - this.DOG_CELLS_HEIGHT * 1.75}

    ];

    this.astroidData = [
        {left: 85,
            top: this.TRACK_2_BASELINE - 1.5 * this.ASTROID_CELLS_HEIGHT},
        {left: 620,
            top: this.TRACK_3_BASELINE},
        {left: 904,
            top: this.TRACK_3_BASELINE - 3 * this.ASTROID_CELLS_HEIGHT},
        {left: 1150,
            top: this.TRACK_2_BASELINE - 3 * this.ASTROID_CELLS_HEIGHT},
        {left: 1720,
            top: this.TRACK_2_BASELINE - 2 * this.ASTROID_CELLS_HEIGHT},
        {left: 1960,
            top: this.TRACK_3_BASELINE - this.ASTROID_CELLS_HEIGHT},
        {left: 2200,
            top: this.TRACK_3_BASELINE - this.ASTROID_CELLS_HEIGHT},
        {left: 2380,
            top: this.TRACK_3_BASELINE - 2 * this.ASTROID_CELLS_HEIGHT}
    ];



    this.lastAnimationFrameTime = 0,
            this.fps = 60,
            this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
            this.lastFpsUpdateTime = 0,
            this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
            this.pausedStartTime;


    this.cat = [];
    this.dog = [];
    this.astroid = [];

    this.runBehaviour =
            {
                lastAdvanceTime: 0,
                execute: function (sprite, now, fps, context,
                        lastAnimationFrameTime)
                {
                    if (sprite.runAnimationRate === 0)
                    {
                        return;
                    }
                    if (this.lastAdvanceTime === 0)
                    {
                        this.lastAdvanceTime = now;
                    } else if (now - this.lastAdvanceTime >
                            1000 / sprite.runAnimationRate)
                    {
                        sprite.artist.advance();
                        this.lastAdvanceTime = now;
                    }
                }
            };

    // Ship explosions.................................................
    this.shipExplodeBehaviour = new CellSwitchBehaviour(
            this.explosionCells,
            this.SHIP_EXPLOSION_DURATION,
            function (sprite, now, fps) { // Trigger
                return sprite.exploding;
            },
            function (sprite, animator) { // Callback
                sprite.exploding = false;
            }
    );

    // Bad guy explosions................................................

    this.badGuyExplodeBehaviour = new CellSwitchBehaviour(
            this.explosionCells,
            this.BAD_GUYS_EXPLOSION_DURATION,
            function (sprite, now, fps) { // Trigger
                return sprite.exploding;
            },
            function (sprite, animator) { // Callback
                sprite.exploding = false;
            }
    );


    this.collideBehaviour =
            {
                isCandidateForCollision: function (sprite, otherSprite)
                {
                    var s, o;
                    s = sprite.calculateCollisionRectangle(),
                            o = otherSprite.calculateCollisionRectangle();
                    return o.left < s.right && sprite !== otherSprite &&
                            sprite.visible && otherSprite.visible && !sprite.exploding &&
                            !otherSprite.exploding;
                },
                didCollide: function (sprite, otherSprite, context)
                {
                    var s, o;
                    s = sprite.calculateCollisionRectangle(),
                            o = otherSprite.calculateCollisionRectangle();

                    //Check whether any of the ship's four corners or centre
                    //lie in the other sprites bounding box
                    context.beginPath();
                    context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);
                    return context.isPointInPath(s.left, s.top) ||
                            context.isPointInPath(s.right, s.top) ||
                            context.isPointInPath(s.centreX, s.centreY) ||
                            context.isPointInPath(s.left, s.bottom) ||
                            context.isPointInPath(s.right, s.bottom);
                },
                processBadGuyCollision: function (sprite)
                {
                    //END GAME or decrease lives
                    starShip.explode(sprite);
                    if (sprite.type === 'ship')
                    {
                        starShip.loseLife();
                    }
                },
                processCollision: function (sprite, otherSprite)
                {

                    if ('cat' === otherSprite.type || 'dog' === otherSprite.type || 'astroid' === otherSprite.type || 'shootRed' === otherSprite.type)
                    {
                        this.processBadGuyCollision(sprite);
                    }
                },
                execute: function (sprite, now, fps, context, lastAnimationFrameTime)
                {
                    var otherSprite;
                    for (var i = 0; i < starShip.sprites.length; ++i)
                    {
                        otherSprite = starShip.sprites[i];
                        if (this.isCandidateForCollision(sprite, otherSprite))
                        {
                            if (this.didCollide(sprite, otherSprite, context))
                            {
                                this.processCollision(sprite, otherSprite);
                            }
                        }
                    }
                }
            };


    this.badyShootBehaviour =
            {
                execute: function (sprite, now, fps, context,
                        lastAnimationFrameTime)
                {
                    var bomb = sprite.bomb,
                            MOUTH_OPEN_CELL = 2;
                    if (!starShip.isSpriteInView(sprite))
                    {
                        return;
                    }
                    if (!bomb.visible && sprite.artist.cellIndex === MOUTH_OPEN_CELL)
                    {
                        bomb.left = sprite.left;
                        bomb.visible = true;
                        starShip.playSound(starShip.cannonSound);
                    }
                }
            };

    this.badyBombMoveBehaviour =
            {
                execute: function (sprite, now, fps, context,
                        lastAnimationFrameTime)
                {
                    var SHOOT_VELOCITY = 550;
                    if (sprite.left + sprite.width > sprite.hOffset
                            && sprite.left + sprite.width < sprite.hOffset +
                            sprite.width)
                    {
                        sprite.visible = false;
                    } else
                    {
                        sprite.left -= SHOOT_VELOCITY *
                                ((now - lastAnimationFrameTime) / 1000);
                    }
                }
            };

};

StarShip.prototype =
        {
            createSprites: function ()
            {

                        this.createDogSprite(),
                        this.createCatSprite(),
                        this.createAstroidSprite(),
                        this.createShipSprite(),
                        this.initializeSprites();
                this.addSpritesToSpriteArray();
            },
            loseLife: function ()
            {
                var TRANSITION_DURATION = 3000;

                this.lives--;
                this.startLifeTransition(starShip.Ship_EXPLOSION_DURATION);

                setTimeout(function () { // After the explosion
                    starShip.endLifeTransition();
                }, TRANSITION_DURATION);
            },
            startLifeTransition: function (slowMotionDelay) {
                var CANVAS_TRANSITION_OPACITY = 0.05,
                        SLOW_MOTION_RATE = 0.1;

                this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
                this.playing = false;

                setTimeout(function () {
                    starShip.setTimeRate(SLOW_MOTION_RATE);
                    starShip.ship.visible = false;
                }, slowMotionDelay);
            },
            endLifeTransition: function () {
                var TIME_RESET_DELAY = 1000,
                        RUN_DELAY = 500;

                starShip.reset();

                setTimeout(function () { // Reset the time
                    starShip.setTimeRate(1.0);

                    setTimeout(function () { // Stop running
                        starShip.ship.runAnimationRate = 0;
                        starShip.playing = true;
                    }, RUN_DELAY);
                }, TIME_RESET_DELAY);
            },
            resetShip: function () {
                this.ship.left = starShip.SHIP_LEFT;
                this.ship.track = 3;
                this.ship.hOffset = 0;
                this.ship.visible = true;
                this.ship.exploding = false;
                this.ship.top = this.ship.height;

                this.ship.artist.cells = this.shipCellsRight;
                this.ship.artist.cellIndex = 0;
            },
            resetOffsets: function () {
                this.bgVelocity = 0;
                this.backgroundOffset = 0;

                this.spriteOffset = 0;
            },
            makeAllSpritesVisible: function () {
                for (var i = 0; i < this.sprites.length; ++i) {
                    this.sprites[i].visible = true;
                }
            },
            reset: function () {
                this.resetOffsets();
                this.resetShip();
                this.makeAllSpritesVisible();
                this.canvas.style.opacity = 1.0;
            },
            positionSprites: function (sprites, spriteData) {
                var sprite;

                for (var i = 0; i < sprites.length; ++i)
                {
                    sprite = sprites[i];
                    sprite.top = spriteData[i].top;
                    sprite.left = spriteData[i].left;

                }
            },
            initializeSprites: function () {
                this.positionSprites(this.dog, this.dogData);
                this.positionSprites(this.cat, this.catData);
                this.positionSprites(this.astroid, this.astroidData);


                this.armShip();
                this.armDog();
                this.armCat();

            },
            setTimeRate: function (rate)
            {
                this.timeRate = rate;
                this.timeSystem.setTransducer(function (now)
                {
                    return now * starShip.timeRate;
                });
            },
            armDogs: function ()
            {
                var dog,
                        shoot = new SpriteSheetArtist(this.spritesheet,
                                this.shootCells);

                for (var i = 0; i < this.dog.length; ++i)
                {
                    dog = this.dog[i];
                    dog.shoot = new Sprite('shoot', shoot, [this.dogShootMoveBehaviour]);

                    dog.shoot.width = starShip.SHOOT_CELLS_WIDTH;
                    dog.shoot.height = starShip.SHOOT_CELLS_HEIGHT;

                    dog.shoot.top = dog.top + dog.bomb.height / 2;
                    dog.shoot.left = dog.left + dog.bomb.width / 2;
                    dog.shoot.visible = false;

                    //Dog shoot maintain a reference to their Dog
                    dog.bomb.dog = dog;
                    this.sprites.push(dog.bomb);
                }
            },
            armCats: function ()
            {
                var cat,
                        shoot = new SpriteSheetArtist(this.spritesheet,
                                this.shootCells);

                for (var i = 0; i < this.cat.length; ++i)
                {
                    cat = this.cat[i];
                    cat.shoot = new Sprite('shoot', shoot, [this.catShootMoveBehaviour]);

                    cat.shoot.width = starShip.SHOOT_CELLS_WIDTH;
                    cat.shoot.height = starShip.SHOOT_CELLS_HEIGHT;

                    cat.shoot.top = cat.top + cat.bomb.height / 2;
                    cat.shoot.left = cat.left + cat.bomb.width / 2;
                    cat.shoot.visible = false;

                    //cat shoot maintain a reference to their cat
                    cat.bomb.dog = cat;
                    this.sprites.push(cat.bomb);
                }
            },
            addSpritesToSpriteArray: function ()
            {


                for (var i = 0; i < this.dog.length; ++i)
                {
                    this.sprites.push(this.dog[i]);
                }

                for (var i = 0; i < this.cat.length; ++i)
                {
                    this.sprites.push(this.cat[i]);
                }

                for (var i = 0; i < this.astroid.length; ++i)
                {
                    this.sprites.push(this.astroid[i]);
                }

                this.sprites.push(this.ship);
            },
            initializeImages: function ()
            {
                this.spritesheet.src = 'images/spritesheet.png';
                this.shipspritesheet.src = 'images/rocket-sprite-sheet.png';
                this.AnimatedGifElement.src = 'images/loading-gif.gif';
                this.background.src = 'images/background.png';

                this.spritesheet.onload = function (e)
                {
                    starShip.backgroundLoaded();
                };

                this.AnimatedGifElement.onload = function ()
                {
                    starShip.loadingAnimationLoaded();
                };
            },
            backgroundLoaded: function ()
            {
                var LOADIND_SCREEN_TRANSITION_DURATION = 2000;
                this.fadeOutElements(this.loadingElement, LOADIND_SCREEN_TRANSITION_DURATION);

                setTimeout(function ()
                {
                    starShip.startGame();
                    starShip.gameStarted = true;
                }, LOADIND_SCREEN_TRANSITION_DURATION);
            },
            spritesheetLoaded: function () {
                var LOADING_SCREEN_TRANSITION_DURATION = 2000;

                this.graphicsReady = true;

                this.fadeOutElements(this.loadingElement,
                        LOADING_SCREEN_TRANSITION_DURATION);

                setTimeout(function () {
                    if (!starShip.gameStarted) {
                        starShip.startGame();
                    }
                }, LOADING_SCREEN_TRANSITION_DURATION);
            },
            shipspritesheetLoaded: function () {
                var LOADING_SCREEN_TRANSITION_DURATION = 2000;

                this.graphicsReady = true;

                this.fadeOutElements(this.loadingElement,
                        LOADING_SCREEN_TRANSITION_DURATION);

                setTimeout(function () {
                    if (!starShip.gameStarted) {
                        starShip.startGame();
                    }
                }, LOADING_SCREEN_TRANSITION_DURATION);
            },
            loadingAnimationLoaded: function ()
            {
                if (!this.gameStarted)
                {
                    this.fadeInElements(this.AnimatedGifElement, this.loadingTitleElement);
                }
            },
            dimControls: function ()
            {
                FINAL_OPACITY = 0.5;
                starShip.instructionsElement.style.opacity = FINAL_OPACITY;
                starShip.soundAndMusicElement.style.opacity = FINAL_OPACITY;
            },
            revealCanvas: function ()
            {
                this.fadeInElements(this.canvas);
            },
            revealTopChrome: function ()
            {
                this.fadeInElements(this.fpsElement, this.scoreElement);
            },
            revealTopChromeDimmed: function ()
            {
                var DIM = 0.25;

                this.scoreElement.style.display = 'block';
                this.fpsElement.style.display = 'block';

                setTimeout(function ()
                {
                    starShip.scoreElement.style.opacity = DIM;
                    starShip.fpsElement.style.opacity = DIM;
                }, this.SHORT_DELAY);
            },
            revealBottomChrome: function ()
            {
                this.fadeInElements(this.soundAndMusicElement,
                        this.instructionsElement, this.copyrightElement);
            },
            revealGame: function ()
            {
                var DIM_CONTROLS_DELAY = 5000;

                this.revealTopChromeDimmed();
                this.revealCanvas();
                this.revealBottomChrome();

                setTimeout(function ()
                {
                    starShip.dimControls();
                    starShip.revealTopChrome();
                }, DIM_CONTROLS_DELAY);
            },
            revealInitialToast: function ()
            {
                var INITIAL_TOAST_DELAY = 1500,
                        INITIAL_TOAST_DURATION = 3000;

                setTimeout(function ()
                {
                    starShip.revealToast('Avoid enemy attack', INITIAL_TOAST_DELAY);
                }, INITIAL_TOAST_DURATION);
            },
            startGame: function ()
            {
                //--------------------------------
                this.revealGame();
                    if(starShip.mobile)
                    {
                       this.fadeInElements(starShip.mobileWelcomeToast);
                    }
                    else
                    {   
                                  this.revealInitialToast();
                    }
                    
                    this.startMusic();
                    this.timeSystem.start();
                    this.setTimeRate(1.0);
                    this.gameStarted = true;
                              window.requestAnimationFrame(this.animate);
                      },

                 startMusic: function()
                 {
                    var MUSIC_DELAY = 1000;

                    setTimeout(function()
                    {
                       if(starShip.musicCheckboxElement.checked)
                       {
                          starShip.musicElement.play();
                       }
                       starShip.pollMusic();
                    }, MUSIC_DELAY);
                 },

                 pollMusic: function()
                 {
                    var POLL_INTERVAL = 500,
                    SOUNDTRACK_LENGTH = 132,
                    timerID;

                    timerID = setInterval(function()
                    {
                       if(starShip.musicElement.currentTime > SOUNDTRACK_LENGTH)
                       {
                          clearInterval(timerID);
                          starShip.restartMusic();
                       }
                    }, POLL_INTERVAL);
                 },

                 restartMusic: function()
                 {
                    starShip.musicElement.pause();
                    starShip.musicElement.currentTime = 0;
                    starShip.startMusic();
                 },

                 createAudioChannels: function()
                 {
                    var channel;

                    for(var i=0; i < this.audioChannels.length; ++i)
                    {
                       channel = this.audioChannels[i];
                       if(i !== 0)
                       {
                          channel.audio = document.createElement('audio');
                          channel.audio.addEventListener('loaddata',
                             this.soundLoaded, false);
                          channel.audio.src = this.audioSprites.currentSrc;
                       }
                       channel.audio.autobuffer = true;
                    }
                 },

                 getFirstAvailableAudioChannel: function()
                 {
                    for(var i =0; i < this.audioChannels.length; ++i)
                    {
                       if(!this.audioChannels[i].playing)
                       {
                          console.log(i);
                          return this.audioChannels[i];
                       }
                    }
                    return null;
                 },

                 seekAudio: function(sound, audio)
                 {
                    try
                    {
                       audio.pause();
                       audio.currentTime = sound.position;
                    }
                    catch(e)
                    {
                       console.error('Cannot play audio');
                    }
                 },

                 playAudio: function(audio, channel)
                 {
                    try
                    {
                       audio.play();
                       channel.playing = true;
                    }
                    catch(e)
                    {
                       if(console)
                       {
                          console.error('Cannot play audio');
                       }
                    }
            },
            animate: function (now)
            {
                now = starShip.timeSystem.calculateGameTime();
                if (starShip.paused)
                {
                    setTimeout(function ()
                    {
                        requestAnimationFrame(starShip.animate);
                    }, starShip.PAUSED_CHECK_INTERVAL);
                } else
                {
                    starShip.fps = starShip.calculateFps(now);
                    starShip.draw(now);
                    starShip.lastAnimationFrameTime = now;
                    requestAnimationFrame(starShip.animate);
                }
            },
            draw: function (now)
            {

                this.setOffsets(now);
                this.drawBackground();
                this.updateSprites(now);
                this.drawSprites();

            },
            setOffSets: function (now)
            {
                this.setBackgroundOffset(now);

            },
            setSpriteOffsets: function (now)
            {
                var sprite;
                this.spriteOffset *
                        (now - this.lastAnimationFrameTime) / 1000;
                for (var i = 0; i < this.sprites.length; ++i)
                {
                    sprite = this.sprites[i];
                    if ('ship' !== sprite.type)
                    {
                        sprite.hOffset = this.spriteOffset;
                    }
                }

            },
            calculateFps: function (now)
            {
                var fps = 1 / (now - this.lastAnimationFrameTime) * 1000;
                if (now - this.lastFpsUpdateTime > 1000)
                {
                    this.lastFpsUpdateTime = now;
                    this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
                }
                return fps;
            },
            setBackgroundOffset: function (now)
            {
                this.backgroundOffset += this.bgVelocity * (now - this.lastAnimationFrameTime) / 1000;
                if (this.backgroundOffset < 0 || this.backgroundOffset > this.background.width)
                {
                    this.bgVelocity = this.bgVelocity * (-1);
                }
            },
            turnLeft: function ()
            {
                this.bgVelocity = -this.STARTING_BACKGROUND_VELOCITY;
            },
            turnRight: function ()
            {
                this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY;
            },
            fadeInElements: function ()
            {
                var args = arguments;
                for (var i = 0; i < args.length; ++i)
                {
                    console.log(args[i]);
                    args[i].style.display = 'block';
                }

                setTimeout(function ()
                {
                    for (var i = 0; i < args.length; ++i)
                    {
                        args[i].style.opacity = starShip.OPAQUE;
                    }
                }, this.SHORT_DELAY);
            },
            fadeOutElements: function ()
            {
                var args = arguments,
                        fadeDuration = args[args.length - 1];
                for (var i = 0; i < args.length - 1; ++i)
                    setTimeout(function () {
                        for (var i = 0; i < args.length - 1; ++i)
                        {
                            args[i].style.display = 'none';
                        }
                    }, fadeDuration);
            },
            hideToast: function ()
            {
                var TOAST_TRANSITION_DURATION = 450;
                this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
            },
            drawBackground: function ()
            {
                this.context.translate(-this.backgroundOffset, 0);
                //Initially Onscreen
                this.context.drawImage(this.background, 0, 0);
                //Initially Offscreen
                this.context.drawImage(this.background, this.background.width, 0);
                this.context.translate(this.backgroundOffset, 0);
            },
            drawShip: function ()
            {
                this.context.drawImage(this.shipImage, this.SHIP_LEFT,
                        this.calculatePlatformTop(this.STARTING_SHIP_TRACK) - this.shipImage.height);
            },
            togglePausedStateOfAllBehaviours: function (now) {

                var behaviour;

                for (var i = 0; i < this.sprites.length; ++i) {
                    sprite = this.sprites[i];

                    for (var j = 0; j < sprite.behaviours.length; ++j) {
                        behaviour = sprite.behaviours[j];

                        if (this.paused) {
                            if (behaviour.pause) {
                                behaviour.pause(sprite, now);
                            }
                        } else {
                            if (behaviour.unpause) {
                                behaviour.unpause(sprite, now);
                            }
                        }
                    }
                }
            },
            togglePaused: function ()
            {
                var now = this.timeSystem.calculateGameTime();
                this.paused = !this.paused;
                this.togglePausedStateOfAllBehaviours(now);
                if (this.paused)
                {
                    this.pauseStartTime = now;
                } else
                {
                    this.lastAnimationFrameTime += (now - this.pauseStartTime);
                }

                if (this.musicOn)
                {
                    if (this.paused)
                    {
                        this.musicElement.pause();
                    } else
                    {
                        this.musicElement.play();
                    }
                }
            },
            revealToast: function (text, duration)
            {
                var DEFAULT_TOAST_DISPLAY_DURATION = 1000;
                duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;

                this.startToastTransition(text, duration);

                setTimeout(function (e)
                {

                    starShip.hideToast();

                }, duration);
            },
            explode: function (sprite)
            {
                if (!sprite.exploding)
                {
                    if (sprite.runAnimationRate === 0)
                    {
                        sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
                    }
                    sprite.exploding = true;
                    starShip.playSound(starShip.explosionSound);
                }
            },
            createDogSprite: function ()
            {
                var dog,
                        dogArtist = new SpriteSheetArtist(this.spritesheet,
                                this.dogCells);

                for (var i = 0; i < this.dogData.length; ++i)
                {
                    dog = new Sprite('dog', dogArtist,
                            [this.paceBehaviour, this.dogShootBehaviour,
                                new CycleBehaviour(300, 1500)]);

                    dog.width = this.CAT_CELLS_WIDTH;
                    dog.height = this.CAT_CELLS_HEIGHT;
                    dog.velocityX = dog.CAT_PACE_VELOCITY;

                    this.dog.push(dog);
                }
            },
            createAstroidSprite: function ()
            {
                var astroid,
                        astroidArtist,
                        ASTROID_FLAP_DURATION = 100,
                        ASTROID_FLAP_INTERVAL = 30;

                for (var i = 0; i < this.astroidData.length; ++i)
                {
                    astroid = new Sprite('astroid', new SpriteSheetArtist(
                            this.spritesheet, this.astroidCells), [new CycleBehaviour(ASTROID_FLAP_DURATION, ASTROID_FLAP_INTERVAL)]);
                    astroid.width = this.ASTROID_CELLS_WIDTH;
                    astroid.height = this.ASTROID_CELLS_HEIGHT;

                    astroid.collisionMargin =
                            {
                                left: 10, top: 10, right: 5, bottom: 10
                            };
                    this.astroid.push(astroid);
                }
            },
            createCatSprite: function ()
            {
                var cat,
                        catArtist = new SpriteSheetArtist(this.spritesheet,
                                this.catCells);

                for (var i = 0; i < this.catData.length; ++i)
                {
                    cat = new Sprite('cat', catArtist,
                            [this.paceBehaviour, this.catShootBehaviour,
                                new CycleBehaviour(300, 1500)]);

                    cat.width = this.CAT_CELLS_WIDTH;
                    cat.height = this.CAT_CELLS_HEIGHT;
                    cat.velocityX = cat.CAT_PACE_VELOCITY;

                    this.cat.push(cat); }
            },
            
   createShipSprite: function()
   {
      var SHIP_LEFT = 50,
          SHIP_HEIGHT = 53,
          STARTING_SHIP_TRACK = 1,
          STARTING_RUN_ANIMATION_RATE = 0;

         this.ship = new Sprite('ship', new SpriteSheetArtist(
         this.spritesheet, this.shipCellsRight), [this.runBehaviour, 
                                                      this.collideBehaviour,this.shipExplodeBehaviour]);
         this.ship.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
         this.ship.track = STARTING_SHIP_TRACK;
         this.ship.left = SHIP_LEFT;
         this.ship.height = SHIP_HEIGHT;
         this.ship.top = this.ship.track - SHIP_HEIGHT;
      
         this.ship.collisionMargin = 
         {
             left: 20,
             top: 15,
             right: 15,
             bottom: 20
          };
          this.sprites.push(this.ship);
    },
            
            
    isSpriteInView: function(sprite)
   {
      //only draw sprite if visible on canvas
      return sprite.left + sprite.width > sprite.hOffset && 
      sprite.left < sprite.hOffset + this.canvas.width;
   },
   //Update all sprites first before drawing 
   //all sprites
   updateSprites: function(now)
   {
      var sprite;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if(sprite.visible && this.isSpriteInView(sprite))
         {
            sprite.update(now, this.fps, this.context,
               this.lastAnimationFrameTime);
         }
      }
   },
   
   putSpriteOnTrack: function(sprite, track) {
      sprite.track = track;
      sprite.top = this.calculatePlatformTop(sprite.track) - 
                   sprite.height;
   },
   etectMobile: function()
   {
      starShip.mobile = 'ontouchstart' in window;
      console.log(starShip.mobile);
   },

   fitScreen: function()
   {
      var arenaSize = starShip.calculateArenaSize(starShip.getViewportSize());
      starShip.resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
   },

   getViewportSize: function()
   {
      return { 
         width: Math.max(document.documentElement.clientWidth 
         || window.innerWidth || 0), 
         height: Math.max(document.documentElement.clientHeight 
         || window.innerHeight || 0)};
   },

   drawMobileInstructions: function()
   {
      var cw = this.canvas.width,
          ch = this.canvas.height,
          TOP_LINE_OFFSET = 115,
          LINE_HEIGHT = 40;

      this.context.save();
      this.initializeContextForMobileInstruction();
      this.drawMobileDivider(cw, ch);
      this.drawMobileInstructionsLeft(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
      this.drawMobileInstructionsRight(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
      this.context.restore();
   },

   initializeContextForMobileInstruction: function()
   {
      this.context.textAlign = 'center';
      this.context.textBaseLine = 'middle';
      this.context.font = '26px fantasy';
      this.context.shadowBlur = 2;
      this.context.shadowOffsetX = 2;
      this.context.shadowOffsetY = 2;
      this.context.shadowColor = 'black';
      this.context.fillStyle = 'yellow';
      this.context.strokeStyle = 'yellow';
   },

   drawMobileDivider: function(cw, ch)
   {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stoke();
   },

   drawMobileInstructionsLeft: function(cw, ch, topLineOffset, lineHeight)
   {
      this.context.font = '32px fantasy';
      this.context.fillText('Slide this way to:', cw/4, ch/2-topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Move left', cw/4,
         ch/2 - topLineOffset + 2 * lineHeight);
   },

   drawMobileInstructionsRight: function(cw, ch, topLineOffset, lineHeight)
   {
      this.context.font = '32px fantasy';
      this.context.fillText('Slide this way to:', 3*cw/4, ch/2-topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Move right', 3*cw/4,
         ch/2 - topLineOffset + 2 * lineHeight);
   },
   
   resizeElementsToFitScreen: function(arenaWidth, arenaHeight)
   {
      starShip.resizeElement(document.getElementById('arena'), arenaWidth,
         arenaHeight);
      starShip.resizeElement(starShip.mobileWelcomeToast, arenaWidth,
         arenaHeight);
      starShip.resizeElement(starShip.mobileStartToast, arenaWidth,
         arenaHeight);
   },

   addTouchEventHandlers: function()
   {
      starShip.canvas.addEventListener('touchstart', starShip.touchStart);
      starShip.canvas.addEventListener('touchend', starShip.touchEnd);
   },

   touchStart: function(e)
   {
      if(starShip.playing)
      {
         e.preventDefault();
      }
   },

   touchEnd: function(e)
   {
      var x = e.changedTouches[0].pageX;
      if(starShip.playing)
      {
         if(x < starShip.canvas.width/2)
         {
            starShip.processLeftTap();
         }
         else if(x > starShip.canvas.width/2)
         {
            starShip.processRightTap();
         }
         e.preventDefault();
      }
   },

   processLeftTap: function()
   {
      if(starShip.runner.direction === starShip.RIGHT)
      {
         starShip.turnLeft();
      }
   },

   processRightTap: function()
   {
      if(starShip.runner.direction === starShip.LEFT)
      {
         starShip.turnRight();
      }
   },

   resizeElement: function(element, w, h)
   {
      element.style.width = w + 'px';
      element.style.height = h + 'px';
   },

   drawSprites: function()
   {
      var sprite;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if(sprite.visible && this.isSpriteInView(sprite))
         {
            this.context.translate(-sprite.hOffset,0);
            sprite.draw(this.context);
            this.context.translate(sprite.hOffset,0);
         }
      }
   }
};
   

//add some vars for keycodes
var KEY_D = 68,
        KEY_LEFT_ARROW = 37,
        KEY_RIGHT_ARROW = 39,
        KEY_K = 75,
        KEY_P = 80;
//Add Event Listener 
window.addEventListener('keydown', function (e)
{
    var key = e.keyCode;
    if (key === KEY_D || key === KEY_LEFT_ARROW)
    {
        starShip.turnLeft();
    } else if (key === KEY_K || key === KEY_RIGHT_ARROW)
    {
        starShip.turnRight();
    }

});
var starShip = new StarShip();
 
 starShip.musicCheckboxElement.addEventListener('change', function (e){
   starShip.musicOn = starShip.musicCheckboxElement.checked;
   if(starShip.musicOn)
   {
      starShip.musicElement.play();
   }
   else
   {
      starShip.musicElement.pause();
   }
});

starShip.welcomeStartLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      starShip.fadeOutElements(starShip.mobileWelcomeToast, FADE_DURATION);
      starShip.playing = true;
   });

starShip.mobileStartLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      starShip.fadeOutElements(starShip.mobileStartToast, FADE_DURATION);
      starShip.mobileInstructionsVisible = false;
      starShip.playing = true;
   });

starShip.showHowLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      starShip.fadeOutElements(starShip.mobileWelcomeToast, FADE_DURATION);
      starShip.drawMobileInstructions();
      starShip.revealMobileStartToast();
      starShip.mobileInstructionsVisible = true;
   });
starShip.initializeImages();
starShip.createSprites();
starShip.createAudioChannels();
starShip.detectMobile();
if(starShip.mobile)
{
   starShip.instructionsElement = document.getElementById('snailbait-mobile-instructions');
   starShip.addTouchEventHandlers();
   
  
  
}

starShip.fitScreen();
window.addEventListener("resize", starShip.fitScreen());
window.addEventListener("orientationchange", starShip.fitScreen());




